#!/bin/bash
# prune_repositories -  remove unused repositories that are not required by any repos
#                       or have no packages from them installed
#
# Copyright (c) 2015 Kylie McClain <somasis@exherbo.org>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#

CAVE=${CAVE:-cave}

if [[ "$1" == "-E" || "$1" == "--environment" ]];then
    shift
    CAVE="$CAVE --environment $1"
    shift
fi

install_repos=( $(${CAVE} print-repositories --format vdb --format exndbam) )
conf_dir=$(${CAVE} print-environment-metadata --raw-name conf_dir --format '%v')

remove_repository() {
    local repository="$1"
    local location=$(${CAVE} print-repository-metadata --raw-name location --format '%v' "${repository}")
    local write_cache=$(${CAVE} print-repository-metadata --raw-name write_cache --format '%v' "${repository}")/"${repository}"
    local names_cache=$(${CAVE} print-repository-metadata --raw-name names_cache --format '%v' "${repository}")/"${repository}"
    [[ -f "${conf_dir}"/repositories/${1}.conf ]] && rm -v "${conf_dir}"/repositories/${1}.conf
    [[ -d "${location}" ]] && rm -vr "${location}"
    [[ -d "${write_cache}" ]] && rm -vr "${write_cache}"
    [[ -d "${names_cache}" ]] && rm -vr "${names_cache}"
    ${CAVE} update-world --remove "repository/${repository}"
}

required_repositories=(
    $(for repo in $(${CAVE} print-repositories --format e);do
        ${CAVE} print-repository-metadata --raw-name master_repository --format '%v\n' "${repo}" | sed "s/^/${repo}:/;s/ /\n${repo}:/g"
    done)
)

required_repositories=(
    $(echo ${required_repositories[@]} | tr ' ' '\n' | sort -ud)
)

prune_repositories=()
for repository in $(${CAVE} print-repositories --format e);do
    ids=()
    echo "$repository:"
    for location in "${install_repos[@]}";do
        ids+=( $(${CAVE} print-ids -f '%W\n' -m "*/*::${repository}->${location}") )
    done
    if [[ -z "${ids[@]}" ]];then
        echo -n "    no packages installed."
        for repo in "${required_repositories[@]}";do
            required_by=()
            if [[ "${repo}" == *:"${repository}" ]];then
                required_by+=( ${repo//:*} )
                repo_unneeded=false
                break
            else
                repo_unneeded=true
            fi
        done
        if [[ "${required_by[@]}" ]];then
            echo -n ".. but, it is needed by ${required_by[*]}"
        fi
        echo
        [[ "${repo_unneeded}" == 'true' ]] && prune_repositories+=( ${repository} )
    else
        echo "    ${#ids[@]} packages installed."
    fi
done
echo

for repository in "${prune_repositories[@]}";do
    if [[ "${1}" == "-y" ]];then
        echo "removing ${repository}"
        remove_repository "${repository}"
    else
        ans=
        while [[ -z "${ans}" ]];do
            ans=
            echo -n "remove ${repository}? [y/N] "
            read -n1 ans
            if [[ -n "${ans}" ]];then
                echo
            else
                ans=n
            fi
            if [[ "${ans,,}" == [yn] ]];then
                if [[ "${ans,,}" == y ]];then
                    remove_repository "${repository}"
                else
                    break
                fi
            else
                ans=
            fi
        done
    fi
done
